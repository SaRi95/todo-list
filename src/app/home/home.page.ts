import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  newItem: string;
  items: Array<string> = [];
  itemCount: number;

  constructor() {}

  ngOnInit() {
    this.itemCount = this.items.length;
  }

  addItem() {
    this.items.push(this.newItem);
    this.itemCount = this.items.length;
    this.newItem = '';
  }

  removeItem(index: number) {
    this.items.splice(index, 1);
    this.itemCount = this.items.length;
  }

}
